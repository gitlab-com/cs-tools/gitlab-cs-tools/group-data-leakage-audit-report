#!/usr/bin/env python3

import gitlab
import json
import csv
import os
import sys
import argparse
from junit_xml import TestSuite, TestCase

def get_project_attributes(gl, projects):
    project_objects = []
    for project in projects:
        project_object = gl.projects.get(project)
        project_objects.append(project_object.attributes)
    return project_objects

def get_group_projects(gl, groups):
    projects = []
    for group in groups:
        group_projects = group.projects.list(as_list=False)
        for group_project in group_projects:
            projects.append(group_project.attributes)
    return projects

def get_subgroups(gl, group, groups):
    print("Getting subgroups for group %s" % group.path)
    groups.append(group)
    subgroups = group.subgroups.list(as_list=False)
    for subgroup in subgroups:
        subgroup_object = gl.groups.get(subgroup.id)
        get_subgroups(gl, subgroup_object, groups)

def check_is_private(groups, projects):
    findings = []
    for group in groups:
        group = group.attributes
        finding = {}
        if group["visibility"] != "private":
            finding["status"] = "failure"
            finding["object"] = "group"
            finding["type"] = "visibility_violation"
            finding["path"] = group["full_path"]
            finding["value"] = group["visibility"]
            finding["description"] = "Group not private: %s has visibility %s" % (group["full_path"], group["visibility"])
            findings.append(finding)
        else:
            finding["status"] = "success"
            finding["object"] = "group"
            finding["type"] = "visibility"
            finding["path"] = group["full_path"]
            finding["description"] = "%s has visibility %s" % (group["full_path"], group["visibility"])
            findings.append(finding)
    for project in projects:
        finding = {}
        if project["visibility"] != "private":
            finding["status"] = "failure"
            finding["object"] = "project"
            finding["type"] = "visibility_violation"
            finding["path"] = project["path_with_namespace"]
            finding["value"] = project["visibility"]
            finding["description"] = "Project not private: %s has visibility %s" % (project["path_with_namespace"], project["visibility"])
            findings.append(finding)
        else:
            finding["status"] = "success"
            finding["object"] = "project"
            finding["type"] = "visibility"
            finding["path"] = project["path_with_namespace"]
            finding["description"] = "%s has visibility %s" % (project["path_with_namespace"], project["visibility"])
            findings.append(finding)
    return findings

def check_forks(gl, projects, group_set):
    findings = []
    for project in projects:
        project_object = gl.projects.get(project["id"], lazy=True)
        forks = project_object.forks.list(as_list=False)
        for fork in forks:
            if fork.attributes["namespace"]["id"] not in group_set:
                finding = {}
                finding["status"] = "failure"
                finding["object"] = "fork"
                finding["type"] = "group_violation"
                finding["path"] = project["path_with_namespace"]
                finding["value"] = "Forked to: %s" % fork.attributes["path_with_namespace"]
                finding["description"] = "Fork outside root group: %s was forked to %s" % (project["path_with_namespace"], fork.attributes["path_with_namespace"])
                findings.append(finding)
            else:
                finding = {}
                finding["status"] = "success"
                finding["object"] = "fork"
                finding["type"] = "group"
                finding["path"] = project["path_with_namespace"]
                finding["description"] = "Fork inside group: %s was forked to %s" % (project["path_with_namespace"], fork.attributes["path_with_namespace"])
                findings.append(finding)
    return findings

def check_shared_projects(gl, projects, group_set):
    findings = []
    for project in projects:
        if project["shared_with_groups"]:
            for group in project["shared_with_groups"]:
                if group["group_id"] not in group_set:
                    finding = {}
                    finding["status"] = "failure"
                    finding["object"] = "project"
                    finding["type"] = "group_share_violation"
                    finding["path"] = project["path_with_namespace"]
                    finding["value"] = "Shared to group: %s" % group["group_full_path"]
                    finding["description"] = "Project shared outside root group: %s was shared with %s, access level %s" % (project["path_with_namespace"], group["group_full_path"], group["group_access_level"])
                    findings.append(finding)
                else:
                    finding = {}
                    finding["status"] = "success"
                    finding["object"] = "project"
                    finding["type"] = "group_share"
                    finding["path"] = project["path_with_namespace"]
                    finding["description"] = "Project shared inside root group: %s was shared with %s, access level %s" % (project["path_with_namespace"], group["group_full_path"], group["group_access_level"])
                    findings.append(finding)
    return findings

def make_test_suite(ts_name, finding_list):
    test_cases = []
    for finding in finding_list:
        name = finding["object"] + " " + finding["type"]
        classname = finding["path"]
        #stdout = finding["value"]
        #stderr = finding["description"]
        case = TestCase(name, classname)
        if finding["status"] == "failure":
            case.add_failure_info(finding["description"])
            case.status = "fail"
        else:
            case.stdout = finding["description"]
        test_cases.append(case)
    return TestSuite(ts_name, test_cases)

def get_failures(test_suite):
    fail_count = 0
    cases = test_suite.test_cases
    for case in cases:
        if case.status == "fail":
            fail_count += 1
    return fail_count

parser = argparse.ArgumentParser(description='Check for data leak in a GitLab.com group')
parser.add_argument('token', help='API token able to read the requested group')
parser.add_argument('group', help='The group to run this report on')
parser.add_argument('--visibility', '-v', help='Run visibility report', action='store_true')
parser.add_argument('--forks', '-f', help='Run visibility report', action='store_true')
parser.add_argument('--projectshares', '-p', help='Run project share report', action='store_true')
args = parser.parse_args()

gitlaburl = "https://gitlab.com/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

group = args.group
group_set = set()
projects = []
fields = []
# writing a manual counter here because status is not easy to get from test cases
fail_count = 0

#get group object and subgroups
top_group = gl.groups.get(group)
groups = []
get_subgroups(gl, top_group, groups)
group_set = set([ group_object.id for group_object in groups ])
projects = get_group_projects(gl, groups)

do_vis = args.visibility
do_forks = args.forks
do_ps = args.projectshares

if not do_vis and not do_forks and not do_ps:
    do_vis = True
    do_forks = True
    do_ps = True

if do_vis:
    visibility_ts = make_test_suite("visibility", check_is_private(groups, projects))
    fail_count += get_failures(visibility_ts)
    with open("visibility_report.xml","w") as reportfile:
        reportfile.write(TestSuite.to_xml_string([visibility_ts]))

if do_forks:
    fork_ts = make_test_suite("forks", check_forks(gl, projects, group_set))
    fail_count += get_failures(fork_ts)
    with open("fork_report.xml","w") as reportfile:
        reportfile.write(TestSuite.to_xml_string([fork_ts]))

if do_ps:
    shared_project_ts = make_test_suite("project_shares", check_shared_projects(gl, projects, group_set))
    fail_count += get_failures(shared_project_ts)
    with open("project_share_report.xml","w") as reportfile:
        reportfile.write(TestSuite.to_xml_string([shared_project_ts]))

#TODO:get groups shared with groups https://gitlab.com/gitlab-org/gitlab/-/issues/205424

#fail the job if we have findings
if fail_count > 0:
    print("Found %s cases of data leakage." % str(fail_count), file=sys.stderr)
    exit(1)