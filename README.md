# Data Leak Report

Check for any potential data leaks of a GitLab.com group:

* Check if all subgroups and projects are private
* Check if projects have no forks outside the group
* Check if projects are not shared outside the group
* Produce junit.xml output to view as test results

## Usage

`python3 data-leakage-audit.py $GIT_TOKEN $GROUP_NAMESPACE`

## Configuration

- Import as new project / fork repository.
- In Settings -> CI/CD -> Variables, add a variable "$GIT_TOKEN" with a GitLab API token as value. That token needs API read rights and permissions to read the group you want to report on.
- edit .gitlab-ci.yml
  - specify your group namespace or id in the $GROUP_ID variable
- Run the Pipeline to get your report

